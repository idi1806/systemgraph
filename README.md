
## systemgraph - a RRDtool frontend for various system statistics by Jochen Schlick
================================================================================

      -----------------------------------------------------
      $Id$
      -----------------------------------------------------


- the cgi script is based on mailgraph's mailgraph.cgi - a RRDtool
  frontend for Postfix Statistics by David Schweikert <dws@ee.ethz.ch>



### Required Packages/Modules
=========================

- rrdtool and
  rrdtool's perl module (RRDs)
  -> http://people.ee.ethz.ch/~oetiker/webtools/rrdtool/

Note: Several Linux distributions have already these modules as RPMs or
      distribution specific packages.
      The following commands work in

       Debian:

           `apt-get install rrdtool`
           `apt-get install librrds-perl`

       Gentoo:

           `emerge rrdtool`   (needs the 'perl' and the 'graph' iuse flag)



#### optional: 
---------
- lsof package

- for detailed network (protocol) statistic:
     `shorewall(version >2.0, http://www.shorewall.net)`
  or
     `iptraf (version >=3.0)`

- ntp tools are necessary for creating the ntpdrift statistic

- lm-sensors package for getting the system health statistic. When
  your health chip isn't supported feel free to send me the output
  of `/usr/bin/sensors <sensorname>` so that I can integrate your sensor
  too. 

- sysstat package (iostat) is needed for harddisk io statistic 

- ipmitool for reading out IPMI/BMC values


NOTE:
- At the moment systemgraph supports only a very limited number of
  system-sensor chips. If your chip is supported by the lm-sensor
  package/kernel and it is not supported by systemgraph feel free
  to send me the output of `/usr/bin/sensors <sensorname>`


- the hddtemp package (see http://www.guzu.net/linux/hddtemp.php)
  is needed if you want to monitor your harddisk temperatures too.
  This package uses the smartctl package for temperature detection and
  therefore your harddisk must be a S.M.A.R.T one (most modern disks
  have this feature).
  For Fedora users:   This package is part of Fedora Extras
  For OpenSUSE users: This package can be found in the packman repository
                      (http://packman.links2linux.org/)

- systemgraph uses the vmstat utility for reading information from the
  proc file system. 


- depending on the version of the lm-sensor package and/or the concerning
  health chip you have to add sometimes parts of the `local.conf.addon` file
  to your own `/etc/sensors.d/local.conf`.


...............................................................................

### Usage
======

systemgraph is made of some scripts:

- rrd_xxx.pl, rrd_xxx.sh
  -----------------------

  These scripts update the RRD databases (database path: `$DATABASEDIR`).
  They must be started periodically. (see `systemgraph.cron.d`) --> `/usr/bin`

- If you want to get a detailed protocol statistic you have from now on
  (systemgraph version 0.9) two possibilities:
  You can use shorewall or iptraf.

  Shorewall is a full featured iptables-based firewall which provides also
  a really easy to use interface for protocol accounting/statistic. If you
  don't know this tool you should try out. It's one of the best open source
  firewall products. http://www.shorewall.net

  - When you want to use shorewall you simply have to modify shorewall's
    accounting config file (`/etc/shorewall/accounting`) for getting detailed
    protocol statistic. There is an example accounting config file in this
    package. This example activates accounting for two interfaces [`eth0` and
    `ppp0`]. You have to adapt these config lines for your interfaces by
    replacing `eth0`/`ppp0` and then insert the whole bunch of lines in your own
    `/etc/shorewall/accounting` config file.
    
  - If you prefer to get a detailed protocol statistic by using iptraf instead
    you need to run an instance of iptraf for every network interface.
    If you don't have pgrep you have to start iptraf manually
    (see `/usr/bin/rrd_iptraf.pl` for more details - in my eyes its behaviour
    is sometimes strange)

  Finally you need to define the interfaces for shorewall / iptraf in your
  `systemgraph.sysconfig` file.

  At the moment the following protocols (UDP/TCP ports) are part of the
  detailed protocol statistic:  
  ```
           #22  ssh
           #25  smtp
           #53  dns
           #80  http
           #110 pop3
           #119 nntp
           #143 imap
           #443 https
           #873 rsync
   ```
   [You can change this by editing the '@ports' line in the `rrd_shorewallstats.pl` /
    `rrd_iptraf.pl` line. Then you have to remove your old rrd-database file because
    it is no longer compatible. The same modifications must done in the systemgraph.cgi
    file's concerning `@graphNetPorts...` lines too.
    When using shorewall you have to modify also the `/etc/shorewall/accounting` file.] 
   


- systemgraph.cron.d
  ------------------

  cron script which starts all shell and perl scripts for updating the RRD
  databases. The database path is defined here as env-variable. --> /etc/cron.d

  It starts every minute the script: `rrd_1minute.sh` (1 minute updates)
            every 5 minutes        : `rrd_5minute.sh` (5 minute updates)



- systemgraph.sysconfig
  ---------------------

  This is a config file that will be read by the RRD database update scripts.
  It holds currently the device names of your local supervised disk devices
  and network devices...                                --> `/etc/sysconfig`
  (!You have to modify this file otherwise you get no disk/network graphs !!!!)
   


- systemgraph.cgi
  ----------------

  This is the CGI script that generates the graphics from the RRD databases.
  Systemgraph.cgi doesn't access the systemgraph.sysconfig file. There are
  some config parameters too [at the beginning of the file], but normally
  there is no need to change these systemgraph.cgi config parameters.
  When you build your RPMs by yourself you are able to modify these params
  via spec-file variables.
  systemgraph.cgi has no idea about your configured hardware/params in your
  systemgraph.sysconfig file ! It simply creates graphs from your existing
  RRD database files. Therefore if you remove for example a formerly
  configured network interface and you don't want to get an empty graph for
  this interface then you have to remove the concerning RRD database file too!

  The default time range in the summary page is 4h[the last 4 hours] but you're
  able to specify your own time range. The unit of the range may be d for
  days, h for hours, m for minutes, s for seconds. See the following
  examples.  



- in your browser type the following: [httpd/apache2 (apache)]
  ----------------------------------

   `http://localhost/sg/systemgraph.cgi`
        -> default statistic = the last 4 hours
  or

   `http://localhost/sg/systemgraph.cgi?100m`
        -> prints the rrdgraphs of the last 100 minutes
  or

   `http://localhost/sg/systemgraph.cgi?20d`
        -> prints the rrdgraphs of the last 20 days

- NOTE: (apache 2.2 / 2.4)
        If you want access the statistic from other hosts than
        localhost/127.0.0.1 you have to modify `/etc/httpd/conf.d/sg.conf`



- in your browser type the following: [boa]
  ----------------------------------

   `http://localhost/cgi-bin/systemgraph.cgi`
        -> prints default statistic = the last 4 hours
  or

   `http://localhost/cgi-bin/systemgraph.cgi?43h`
        -> prints the rrdgraphs of the last 43 hours
  or

   `http://localhost/cgi-bin/systemgraph.cgi?43d`
        -> prints the rrdgraphs of the last  43 days



...............................................................................


### Manual Installation (tgz-file/tar.bz2-file)
==============================

#### for the rrd-database generation:


1) copy `*.sh` --> `/usr/bin`

2) copy `*.pl` --> `/usr/bin`


3) copy `systemgraph.sysconfig` -> `/etc/sysconfig/` and modify it

4) create the rrd-database directory. The default value for this directory
   is `/var/lib/systemgraph`.

5) copy `systemgraph.cron.d` --> `/etc/cron.d/`  (uses `DATABASEDIR=/var/lib/systemgraph`)

Tama Pugsley reported that the .crond.d-ending didn't work (in Debian). The
name of this file is completely irrelevant. So feel free to rename it when
your crond daemon has problems with the original name.

NOTE: If you want that the created rrd-database files will be placed
      somewhere else than in `/var/lib/systemgraph`:
      - then edit the `DATABASEDIR` environment variable in `systemgraph.cron.d`.
      - But be aware to edit also the corresponding variable
        (`$systemgraphRRDDir`) in the systemgraph.cgi file. 
      - the directory must exist and must be accessible.

6) restart crond
```
   Fedora:         service crond restart
   OpenSUSE:       service cron  restart
   Debian/Ubuntu:  sudo /etc/init.d/cron restart
   Gentoo:         /etc/init.d/vixie-cron restart
```


#### for cgi installation: (apache) 


7) copy systemgraph.cgi to a place where it is accessible and excutable by
   your local webserver, and where it has access to the generated
   rrd-databases.

NOTE1: Don't forget to edit the `$systemgraphRRDir` variable in systemgraph.cgi
       when your rrd-databases won't be created in the default directory
       (`/var/lib/systemgraph`)

NOTE2: If the default font string (`# font` which is part of the rrdtool
       package (1.2.x)) didn't work comment that line and uncomment the
       `my @FFont = ();` line.


8) copy the file `systemgraph.httpd.conf` to `/etc/httpd/conf.d/` (Fedora) 
   or `/etc/apache2/conf.d/` (OpenSUSE)

   [Automatic RPM install creates a directory `/var/www/sg` and copies
    systemgraph.cgi to this directory. Then it copies the file
    systemgraph.httpd.conf to the distribution specific apache config
    directory (`/etc/httpd/conf.d, /etc/apache2/conf.d`) ]

   or simply add the following four lines to your virtual host
   instead of using the whole file.

```
  <Directory "[directory where cgi is]">
   Options +ExecCGI
   AddHandler cgi-script .cgi .pl
  </Directory>
```
   


9) restart httpd (apache)
....




#### for cgi installation: (boa)


7) simply copy systemgraph.cgi to boa's cgi-bin directory (see
   `/etc/boa/boa.conf`)

8) restart boa
```
  Fedora:          service crond restart
  OpenSUSE:        service cron  restart
  Debian/Ubuntu:   sudo /etc/init.d/boa restart
  Gentoo:          /etc/init.d/boa restart
```

PS: if you play around with different webservers be aware to remove
    the `/tmp/systemgraph` directory before starting with a new webserver
    because every webserver uses its own `user:group` when creating the
    png-files in this directory. Therefore another webserver would fail
    when he has not the same user:group ...

    [The default tmp-directory of systemgraph.cgi is `/tmp/systemgraph`]


...............................................................................


### Directories and files: (after Fedora/Redhat - RPM-installation)
================================================================

```
systemgraph-<version>.noarch.rpm:
---------------------------------

/var/www/sg/                           all *.rrd-Files (database files)
                                       (instead of default /var/lib/systemgraph)

/usr/bin/rrd_*.sh                      rrd database update scripts
/usr/bin/rrd_*.pl

/etc/cron.d/systemgraph.cron.d         cronscript

/etc/sysconfig/systemgraph.sysconfig   configfile for the rrd_*.sh/.pl
                                       database scripts


systemgraph-httpd-<version>.noarch.rpm:
---------------------------------------

/etc/httpd/conf.d/sg.conf              apache systemgraph configuration...
/var/www/sg/systemgraph.cgi            the cgi-script

/tmp/systemgraph-httpd/                all created *.png-files (by apache)


systemgraph-boa-<version>.noarch.rpm:
-------------------------------------

/usr/lib/boa/cgi-bin/systemgraph.cgi   the cgi-script in boa's cgi-bin 

/tmp/systemgraph-boa/                  all created *.png-files (by boa)
```


............................................................................

### Directories and files: (after Opensuse - RPM-installation)
===========================================================

```
systemgraph-<version>.noarch.rpm:
---------------------------------

/var/www/sg/                           all *.rrd-Files (database files)
                                       (instead of default /var/lib/systemgraph)

/usr/bin/rrd_*.sh                      rrd database update scripts
/usr/bin/rrd_*.pl

/etc/cron.d/systemgraph.cron.d         cronscript

/etc/sysconfig/systemgraph.sysconfig   configfile for the rrd_*.sh/.pl
                                       database scripts



systemgraph-apache2-<version>.noarch.rpm:
---------------------------------------

/etc/apache2/conf.d/sg.conf            apache systemgraph configuration...
/var/www/sg/systemgraph.cgi            the cgi-script

/tmp/systemgraph-apache2/              all created *.png-files (by apache)




systemgraph-boa-<version>.noarch.rpm:
-------------------------------------

/usr/lib/boa/cgi-bin/systemgraph.cgi   the cgi-script in boa's cgi-bin 

/tmp/systemgraph-boa/                  all created *.png-files (by boa)
```


............................................................................

### Directories and files: (after Gentoo - systemgraph-xx.ebuild -installation)
===========================================================================

NOTE0: The ebuild is still very experimental. It's my first one!!
       Whether it works without manual intervention or not depends
       on your webserver. I'm looking for a good solution which is
       independant of the installed webserver...

NOTE1: For full embedding into Gentoo the systemgraph.ebuild replaces
       some path names. (see below)

```
systemgraph-<version>.ebuild
-----------------------------

/var/lib/systemgraph/                  all *.rrd-Files (database files)

/usr/bin/rrd_*.sh                      rrd database update scripts
/usr/bin/rrd_*.pl

/etc/cron.d/systemgraph.cron.d         cronscript

/etc/conf.d/systemgraph.sysconfig      configfile for the rrd_*.sh/.pl
                                       database scripts


${MY_CGIBINDIR}/systemgraph.cgi        the cgi-script in cgi-bin 

/tmp/systemgraph/                      all created *.png-files
```


............................................................................


### Slackware Linux
================

Many thanx to Leonardo Roman who created and kindly provided a
systemgraph.SlackBuild script for Slackware Linux. This build
script is part of the systemgraph package since version 0.9.7.5).


To create your own Slackware systemgraph package:

```
>  systemgraph.SlackBuild  <systemgraph-XXX.tar.bz2-file> 
```


............................................................................


### OpenBSD
=======

Most of the stuff runs also with OpenBSD (tested with OpenBSD 4.4). After
you have installed the package you only have to activate the concerning
OpenBSD lines in `/etc/cron.d/systemgraph.cron.d`



............................................................................



### Thanx to
========
```
Vincent Deffontaines            - basic cgi parameter checking
                                - patch to support w83697hf sensor 

Thierry Daucourt                - patch to support via686a-isa-6000 sensor.
                                - some performance patches.
                                - running systemgraph on Ubuntu

Brad Lhotsky                    - patch to support it87_i2c_0_2d

Tama Pugsley                    - running systemgraph on Debian.
                                - useful comments 
                                
Jamie Scott                     - info about health chip it87-isa-0290
Eddie Bruce                     - info about health chip w83627thf-isa-0290

Johan Willard                   - patch to support HP DL360 G5 server's disk
                                  devices for hdstat.

Louis B. Moore                  - info about health chip w83792d-i2c-0-2f

Aki Parviainen                  - cgi addon

Blaine Anaya                    - OpenBSD 4.4

Leonardo Roman                  - running systemgraph on Slackware and
                                  provides a package build script for
                                  Slackware.  

Jeff Rickman                    - patch to support atk0110-acpi-0,
                                  f71805f-isa-0290, via_cputemp-isa-0000
                                  + /etc/sensors.d/local.conf - addon for the
                                  Fintek health chip.

Otto Mueller                    - fscher-i2c-0-73 sensor
```

............................................................................


### License
=======
```
    This file is part of systemgraph.

    systemgraph is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    systemgraph is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with systemgraph.  If not, see <http://www.gnu.org/licenses/>.

    See the file LICENSE included in the distribution for details.
```

