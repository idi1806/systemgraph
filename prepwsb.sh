#!/bin/sh
#
#    This file is part of systemgraph.
#
#    Copyright (C) 2004 - 2018,  Jochen Schlick 
#
#    systemgraph is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    systemgraph is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with systemgraph.  If not, see <http://www.gnu.org/licenses/>.
#
# $Id$
##############################################################################

VERSION=`grep 'VERSION \=' systemgraph.cgi | cut -d' ' -f4 | sed s/\"//g`
PACKAGE_NAME="systemgraph"
PACKAGE_N_V="${PACKAGE_NAME}-${VERSION}"


WSB="${HOME}/src/git/wsb3902919501"


echo "prepwsb.sh: version=${VERSION}"
echo ""



echo "copy ChangeLog, README to ${WSB}/sw/systemgraph"
cp -pf ChangeLog ${WSB}/sw/systemgraph
cp -pf README.md ${WSB}/sw/systemgraph/README

echo "copy tar, slackbuild and ebuild to ${WSB}/sw/systemgraph"
cp -pf systemgraph-99999999.ebuild     ${WSB}/sw/systemgraph
cp -pf ${PACKAGE_N_V}.ebuild           ${WSB}/sw/systemgraph
cp -pf ${PACKAGE_NAME}.SlackBuild      ${WSB}/sw/systemgraph
cp -pf ${PACKAGE_N_V}.tar.bz2          ${WSB}/sw/systemgraph

