#!/bin/sh
#
#    This file is part of systemgraph.
#
#    Copyright (C) 2004-2018 Jochen Schlick 
#
#    systemgraph is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    systemgraph is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with systemgraph.  If not, see <http://www.gnu.org/licenses/>.
#
# $Id$
##############################################################################

VERSION=`grep 'VERSION \=' systemgraph.cgi | cut -d' ' -f4 | sed s/\"//g`
PACKAGE_NAME="systemgraph"
PACKAGE_N_V="${PACKAGE_NAME}-${VERSION}"

echo "preprpm.sh: version=${VERSION}"
echo ""

rm -rf ${PACKAGE_N_V}
rm -f ${PACKAGE_NAME}-*.tgz
rm -f ${PACKAGE_NAME}-*.tar.bz2
rm -f ${PACKAGE_NAME}-*.*.ebuild
rm -f ${PACKAGE_NAME}-*.spec
cp   ${PACKAGE_NAME}.ebuild    ${PACKAGE_N_V}.ebuild
cp   ${PACKAGE_NAME}.spec      ${PACKAGE_N_V}.spec

# spec contains version placeholder... replace it
perl -pi -e "s/VERSION_PLACEHOLDER/${VERSION}/" ${PACKAGE_N_V}.spec

mkdir ${PACKAGE_N_V}
cp -f * ${PACKAGE_N_V}/

rm -f ${PACKAGE_N_V}/${PACKAGE_NAME}.ebuild
rm -f ${PACKAGE_N_V}/${PACKAGE_NAME}.spec
rm -f ${PACKAGE_N_V}/prepebuild.sh
rm -f ${PACKAGE_N_V}/preprpm.sh
rm -f ${PACKAGE_N_V}/prepwsb.sh
rm -f ${PACKAGE_N_V}/test_rrd*
rm -f ${PACKAGE_N_V}/*~
rm -f ${PACKAGE_N_V}/*.bak

tar cvjf ~/rpmbuild/${PACKAGE_NAME}/${PACKAGE_N_V}.tar.bz2 ${PACKAGE_N_V}/
rm -rf ${PACKAGE_N_V}

echo "rpmbuild -ba ./${PACKAGE_N_V}.spec"

