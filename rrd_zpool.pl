#!/usr/bin/perl
#             rrd_zpool.pl  poolname1 [poolname2 ....]
#               or
#             rrd_zpool.pl  <without parameters>
#                           in this case we get the poolnames from
#                           /etc/conf.d/systemgraph.sysconfig
#
#
# extended poolname format:
#
#  <poolName>:<extendedName=mountpoint or other useful name>
#
# Allowed chars for <poolName>     a-zA-Z0-9-_/:
# Allowed chars for <extendedName> a-zA-Z0-9-_/. (no :)
#
# -where all '/' will be replaced internally by ','
# -where all ':' except the last ':' will be replaced by '|'
#
#            example: (without extended poolname format)
#             rrd_zpool.pl  tank800 tank326
#
#
#            example: (with extended poolname format)
#             rrd_zpool.pl  tank800:/DATA/tank800 tankUSR:/usr  tank45:blablub
#
#
# NOTE: a too long extended name results in the
#       following error message in your http
#       server's errorlog file:
#                    "basicCheckParam: [....]"
#
#
##############################################################################
#
#    This file is part of systemgraph.
#
#    Copyright (C) 2020-2021 Jochen Schlick
#
#    systemgraph is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    systemgraph is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with systemgraph.  If not, see <http://www.gnu.org/licenses/>.
#
#
# $Id$
##############################################################################
use strict;
use RRDs;

# NOTE: uses the same prefix as rrd_disk.pl
my $rrdDatabaseName = 'disk.rrd.';
my $sysconfigFile   = '/etc/conf.d/systemgraph.sysconfig';
#
#..............................................................................
# path to hddtemp is either in /usr/bin, /usr/bin, /usr/local/bin
my $pgm      = '';
my $pgmName  = 'zpool';

# check whether nothing to do or not
$ENV{'PATH'} = '/sbin:/usr/sbin:/usr/local/sbin';
for my $path ( split /:/, $ENV{PATH} ) {
  if ( -f "$path/$pgmName" ) {
    #print STDERR "$pgmName found in $path\n";
    $pgm     =  "$path/$pgmName";
    last;
  }
}
exit(0) if ($pgm eq '');
#
#..............................................................................
# database dir env exist ?
my $rrdDatabasePath;

if (exists $ENV{'DATABASEDIR'} ) {
  $rrdDatabasePath = $ENV{'DATABASEDIR'} . "/" . "$rrdDatabaseName";
} else {
  $rrdDatabasePath = '/var/lib/systemgraph/' . "$rrdDatabaseName";
}
#print STDERR "$rrdDatabasePath";

#..............................................
my @rrdPools;
if ((scalar @ARGV) > 0) {
  @rrdPools = @ARGV;
} else {

  # open input file
  open(inFH, "< $sysconfigFile")  or usage();

  while (defined (my $inLine = <inFH>)) {

    #print STDERR "$inLine";

    if      ($inLine =~/^ZPOOL=/o) {

      (my $poolname = $inLine) =~ s/^ZPOOL=//og;
      chomp $poolname;

      push(@rrdPools, $poolname);
    }
  }

  close(inFH);
}

#extract names

#print STDERR join("\n", @rrdPools). "\n";

# to avoid a lot of emails from cron when not configured
if (exists $ENV{'RRD_SILENT'}) {
  exit 0  if ((scalar @rrdPools) == 0);
} else {
  usage() if ((scalar @rrdPools) == 0);
}

#..............................................................................

# fill the databases
fill_rrd ();

#..............................................................................
sub usage {

  my $pgmName = $0;
  $pgmName    =~ s/.*\///;  # remove path

  print STDERR <<ENDOFUSAGETEXT;

usage: $pgmName poolname1 [poolname2 ....]
        or
       $pgmName <without parameters>
        in this case the poolnames(partitionname) must be defined
        in $sysconfigFile

 extended poolname format:

  <pool>:<mountpoint or other useful name>

            example: (without extended poolname format)
             rrd_zpool.pl  tank800 tank326 tankUSR

            example: (with extended poolname format)
             rrd_zpool.pl  tank800:/DATA/tank800 tankUSR:/usr tank326:blablub


ENDOFUSAGETEXT

  exit 1;
}
#..............................................................................
sub create_rrd($) {

  my $rrdFile = $_[0];
  #print STDERR "create: [$rrdFile]\n";

  # awaiting an update every 300 secs
  my $rrdStep = 300;

  # data source = ...GAUGE, max 600 sec wait before UNKNOWN,
  #   no min, no max
  # 0.5:1: average value calc. with 1 entry = 300sec
  # 0.5:5: average value calc. with 5 entries = 5*300sec

  # h36    = 3600*36     => 129600sec/300      => 432
  # d7a2   = 3600*24*7   => 604800sec/300 /2   => 1008; 10min average
  # d30a6  = 3600*24*30  => 2592000sec/300 /6  => 1440; 30min average
  # d365a12= 3600*24*365 => 31536000sec/300 /12=> 8760; 1h    average

  # d7a6   = 3600*24*7   => 604800sec/300 /6   => 336;  30min max
  # d30a12 = 3600*24*30  => 2592000sec/300 /12 => 720;  1h    max
  # d365a24= 3600*24*365 => 31536000sec/300 /24=> 4380; 2h    max

  RRDs::create("$rrdFile",
               '--step', $rrdStep,

               'DS:d:GAUGE:'     .($rrdStep*2).':0:U',
               'DS:avail:GAUGE:' .($rrdStep*2).':0:U',

               'RRA:AVERAGE:0.5:1:432',
               'RRA:AVERAGE:0.5:2:1008',
               'RRA:AVERAGE:0.5:6:1440',
               'RRA:AVERAGE:0.5:12:8760',

               'RRA:MAX:0.5:6:336',
               'RRA:MAX:0.5:12:720',
               'RRA:MAX:0.5:24:4380'
              );

  my $ERR = RRDs::error;
  die "ERROR while creating $rrdFile: $ERR\n" if $ERR;
}

#..............................................................................
sub update_rrd ($$$$){

  my $rrdFile  = $_[0];

  #my $dataTime = $_[1];
  #my $used     = $_[2];
  #my $avail    = $_[3];

  #print STDERR "update: [$rrdFile], dataTime=$_[1], used=$_[2], avail=$_[3]\n";

  RRDs::update("$rrdFile", $_[1].':'.$_[2].':'.$_[3]);
  my $ERR = RRDs::error;
  die "ERROR while updating $rrdFile: $ERR\n" if $ERR;
}

#..............................................................................
sub fill_rrd () {

  #print STDERR @rrdPools;

  # extract the data from df
  my $rrdTime = time;

  #zpool list  -Ho name,allocated,free
  #tank326 214G 108G
  #tank40 15.3G 23.7G
  #tank60 40.5G 21.0G
  #
  #zpool list -p  -Ho name,allocated,free
  #tank326 230062759936 115682107392
  #tank40 16473321472 25402609664
  #tank60 43484131328 22550990848
  #
  foreach my $rrdPool (@rrdPools) {

    my $realPool;
    my $extDev   ='';
    #                     ':' is  the last one in the line
    if ($rrdPool =~ /^(.+)\:(.+)/o) {
      $realPool  = $1;
      $extDev    = '.'. $2;
    } else {
      $realPool   = $rrdPool;
    }

    my @dfLines = `$pgm list -p -Ho allocated,free $realPool 2>&1`;

    #zpool list -p  -Ho allocated,free tank326
    #230078078976 115666788352

    # max. 1 line left
    foreach my $dfLine (@dfLines) {

      if ($dfLine  =~ /(\d+)+\s+(\d+)/ ) {

        my $usedM  = sprintf("%d",$1/1024);
        my $availM = sprintf("%d",$2/1024);
        my $usedM  = sprintf("%d",$usedM/1024);
        my $availM = sprintf("%d",$availM/1024);

        #print STDERR "realPool=[$realPool] used:  $1  / 1024 / 1024 = $usedM\n";
        #print STDERR "realPool=[$realPool] avail: $2  / 1024 / 1024 = $availM\n";

        # convert '/' to ','
        $realPool   =~ s:/:\,:og;
        # convert ':' to '|'
        $realPool   =~ s/:/\|/og;

        # convert '/' to ','
        $extDev    =~ s:/:\,:og;

        my $rrdFile  = "$rrdDatabasePath" . $realPool . $extDev;
        #print STDERR "FND[$dfLine]: rrdFile=[$rrdFile], realPool=[$realPool], extDev=[$extDev]\n";

        # fill database
        create_rrd($rrdFile) if not -w $rrdFile;

        update_rrd($rrdFile,
                   $rrdTime,
                   $usedM,
                   $availM)  if -w $rrdFile;
      }
    }
  }
}

#..............................................................................

