#!/bin/sh
#    This file is part of systemgraph.
#
#    Copyright (C) 2004-2014 Jochen Schlick 
#
#    systemgraph is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    systemgraph is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with systemgraph.  If not, see <http://www.gnu.org/licenses/>.
#
# $Id$
##############################################################################
#
#         - testing the rrd_health.pl with real
#           sensor data output
#         - testing test_rrd_iptraf.pl
#
####################################################
cd testdata1
echo "TESTDATA1"
mkdir -p ../testout
#
####################################################
DATABASEDIR=../testout ../../rrd_health.pl TEST acpitz-virtual-0
DATABASEDIR=../testout ../../rrd_health.pl TEST acpitz-virtual-0::2
DATABASEDIR=../testout ../../rrd_health.pl TEST acpitz-virtual-0::4
DATABASEDIR=../testout ../../rrd_health.pl TEST acpitz-virtual-0::6
DATABASEDIR=../testout ../../rrd_health.pl TEST acpitz-virtual-0::8
DATABASEDIR=../testout ../../rrd_health.pl TEST acpitz-virtual-0::9
DATABASEDIR=../testout ../../rrd_health.pl TEST adm1027-i2c-0-2e
DATABASEDIR=../testout ../../rrd_health.pl TEST asb100-i2c-0-2d
DATABASEDIR=../testout ../../rrd_health.pl TEST atk0110-acpi-0
DATABASEDIR=../testout ../../rrd_health.pl TEST coretemp-isa-0000
sleep 1
DATABASEDIR=../testout ../../rrd_health.pl TEST coretemp-isa-0000::1
DATABASEDIR=../testout ../../rrd_health.pl TEST coretemp-isa-0000::2
DATABASEDIR=../testout ../../rrd_health.pl TEST coretemp-isa-0000::5
DATABASEDIR=../testout ../../rrd_health.pl TEST coretemp-isa-0001
DATABASEDIR=../testout ../../rrd_health.pl TEST dme1737-i2c-0-2e
DATABASEDIR=../testout ../../rrd_health.pl TEST f71862fg-isa-0220
DATABASEDIR=../testout ../../rrd_health.pl TEST f71862fg-isa-0a00
DATABASEDIR=../testout ../../rrd_health.pl TEST f71805f-isa-0290
DATABASEDIR=../testout ../../rrd_health.pl TEST fscher-i2c-0-73
DATABASEDIR=../testout ../../rrd_health.pl TEST i350bb-pci-0300
sleep 1
DATABASEDIR=../testout ../../rrd_health.pl TEST i350bb-pci-0300::1
DATABASEDIR=../testout ../../rrd_health.pl TEST i350bb-pci-0300::2
DATABASEDIR=../testout ../../rrd_health.pl TEST it87-i2c-0-2d
DATABASEDIR=../testout ../../rrd_health.pl TEST it87-isa-0290
DATABASEDIR=../testout ../../rrd_health.pl TEST it8716-isa-0228
DATABASEDIR=../testout ../../rrd_health.pl TEST it8716-isa-0290
DATABASEDIR=../testout ../../rrd_health.pl TEST it8718-isa-0228
DATABASEDIR=../testout ../../rrd_health.pl TEST it8718-isa-0290
DATABASEDIR=../testout ../../rrd_health.pl TEST it8720-isa-0228
DATABASEDIR=../testout ../../rrd_health.pl TEST k8temp-pci-00c3
DATABASEDIR=../testout ../../rrd_health.pl TEST k8temp-pci-00c3::2
DATABASEDIR=../testout ../../rrd_health.pl TEST k8temp-pci-00c3::4
DATABASEDIR=../testout ../../rrd_health.pl TEST k10temp-pci-00c3
DATABASEDIR=../testout ../../rrd_health.pl TEST k10temp-pci-00c3::2
sleep 1
DATABASEDIR=../testout ../../rrd_health.pl TEST k10temp-pci-00c3::1
DATABASEDIR=../testout ../../rrd_health.pl TEST k8temp-pci-00c3
DATABASEDIR=../testout ../../rrd_health.pl TEST k8temp-pci-00c3::2
DATABASEDIR=../testout ../../rrd_health.pl TEST lm85b-i2c-0-2e
DATABASEDIR=../testout ../../rrd_health.pl TEST nct6792-isa-0a20
DATABASEDIR=../testout ../../rrd_health.pl TEST ne1619-i2c-0-2d
DATABASEDIR=../testout ../../rrd_health.pl TEST smsc47b397-isa-0480
DATABASEDIR=../testout ../../rrd_health.pl TEST smsc47m1-isa-0800
DATABASEDIR=../testout ../../rrd_health.pl TEST via686a-isa-6000
DATABASEDIR=../testout ../../rrd_health.pl TEST via_cputemp-isa-0000
DATABASEDIR=../testout ../../rrd_health.pl TEST w83627dhg-isa-0290
DATABASEDIR=../testout ../../rrd_health.pl TEST w83627ehf-isa-0290
DATABASEDIR=../testout ../../rrd_health.pl TEST w83627hf-isa-0290
DATABASEDIR=../testout ../../rrd_health.pl TEST w83627thf-isa-0290
DATABASEDIR=../testout ../../rrd_health.pl TEST w83697hf-isa-0290
DATABASEDIR=../testout ../../rrd_health.pl TEST w83781d-i2c-0-2d
DATABASEDIR=../testout ../../rrd_health.pl TEST w83791d-i2c-1-2d
DATABASEDIR=../testout ../../rrd_health.pl TEST w83792d-i2c-0-2f
DATABASEDIR=../testout ../../rrd_health.pl TEST w83l771-i2c-0-4c
####################################################
cd ../testdata2
echo "TESTDATA2"
sleep 1
####################################################
DATABASEDIR=../testout ../../rrd_health.pl TEST coretemp-isa-0000
DATABASEDIR=../testout ../../rrd_health.pl TEST coretemp-isa-0001
DATABASEDIR=../testout ../../rrd_health.pl TEST i350bb-pci-0400::1
DATABASEDIR=../testout ../../rrd_health.pl TEST k8temp-pci-00c3
DATABASEDIR=../testout ../../rrd_health.pl TEST k8temp-pci-00c3::2
DATABASEDIR=../testout ../../rrd_health.pl TEST nvme-pci-0100::3
DATABASEDIR=../testout ../../rrd_health.pl TEST w83627ehf-isa-0290 
DATABASEDIR=../testout ../../rrd_health.pl TEST w83627hf-i2c-0-2d
####################################################
cd ../testdata3
echo "TESTDATA3"
sleep 1
####################################################
DATABASEDIR=../testout ../../rrd_health.pl TEST IPMITOOL:1:5
DATABASEDIR=../testout ../../rrd_health.pl TEST asus-isa-0000
DATABASEDIR=../testout ../../rrd_health.pl TEST coretemp-isa-0000::3
DATABASEDIR=../testout ../../rrd_health.pl TEST coretemp-isa-0000::5
DATABASEDIR=../testout ../../rrd_health.pl TEST k8temp-pci-00c3
DATABASEDIR=../testout ../../rrd_health.pl TEST k8temp-pci-00c3::2
DATABASEDIR=../testout ../../rrd_health.pl TEST w83627ehf-isa-0290 
####################################################
cd ..
####################################################
DATABASEDIR=./testout ./test_rrd_iptraf.pl eth0
###############################################################################
