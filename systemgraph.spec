# $Id$

######################### modified by script ##############
%define systemgraph_version VERSION_PLACEHOLDER
######################### modified by script ##############

%if 0%{?fedora}
%define plat       fc
%define apache     httpd
%define crondaemon crond
%else
%if 0%{?rhel_version}
%define plat       rh
%define apache     httpd
%define crondaemon crond
%else
%if 0%{?centos_version}
%define plat       co
%define apache     httpd
%define crondaemon crond
%else
%if 0%{?suse_version}
%define plat       os
%define apache     apache2
%define crondaemon cron
%else
%if 0%{?sles_version}
%define plat       sl
%define apache     apache2
%define crondaemon cron
%else
%define plat       rh
%define apache     httpd
%define crondaemon crond
%endif
%endif
%endif
%endif
%endif

#......................................................................

Name:           systemgraph
Version:        %{systemgraph_version}
Release:        50.js.%{plat}
Epoch:          0
Summary:        A RRDtool Frontend For Various System Statistics

Group:          System Environment/Daemons
License:        GPLv3
URL:            http://www.decagon.de/sw/systemgraph/
Source0:        %{name}-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  perl 
Requires:       perl
Requires:       rrdtool
Requires:       lsof
Requires:       gawk

BuildArch:      noarch


%define ownwwwDir  sg
%define _wwwDir    /var/www/%{ownwwwDir}
%define _rrdDir    %{_wwwDir}

%if 0%{?suse_version}
# boa's default cgi directory
%define _boaCgiDir /usr/lib/boa/cgi-bin
%else
# fedora's boa version uses this one
%define _boaCgiDir /var/www/boa/cgi-bin
%endif


# config options: systemgraph.cgi
%define _withRRDTOOL_GIF  0
%define _dieOnParamErrors 1
%define _pngXPoints       500


#.....................................................................
%description
systemgraph is a very simple statistics RRDtool frontend for various
system specific values (loadavg, used memory, processes...)
that produces daily, weekly, monthly and yearly graphs.
 - ntp drift (depends on installed ntp)
 - network devices, traffic, protocol statistic, proxy (privoxy) statistic
 - disk devices (size, io performance)
 - open files (needs an installed lsof)
 - cpu infos, memory (needs vmstat >=3)
 - temperature,fan status .... (needs an installed lm_sensors package...)
 - users

If you want to get a detailed protocol statistic you have now two
possibilities. You can use shorewall(www.shorewall.net) or iptraf.
- When you want to use shorewall you simply have to modify shorewall's
  accounting file (/etc/shorewall/accounting) for getting the detailed
  protocol statistic. There is an example accounting file for eth0 and
  ppp0 which is part of this package. You have to adapt this for other
  interfaces by replacing eth0/ppp0 and then insert these lines in your
  own /etc/shorewall/accounting file.
- If you prefer to get the detailed protocol statistic by using iptraf instead
  you need to run an instance of iptraf for every network interface.
  If you don't have pgrep you have to start iptraf manually
  (see /usr/bin/rrd_iptraf.pl for more details - in my eyes its behaviour
  is sometimes strange)
Finally you need to define the interfaces for shorewall / iptraf in your
systemgraph.sysconfig file.

NOTE: the cgi-file is now part of the concerning systemgraph-webserver
      (%{apache}/boa) package ! Therefore you need to install at least one of the
      systemgraph-webserver packages.



%package %{apache}
Group:   System Environment/Daemons
Summary: A RRDtool Frontend For Various System Statistics for the Apache HTTP server.
Requires: %{apache}
Requires: systemgraph = %{version}-%{release}
Provides: systemgraph-webconfig

%description %{apache}
This package holds the systemgraph.cgi file and the configuration
for the apache web server.

It is also possible to define an own time range for the global systemgraph
summary page: http://localhost/sg/systemgraph.cgi?43h
                  --> summary for the last 43 hours
              http://localhost/sg/systemgraph.cgi?43d
                  --> summary for the last 43 days


%package boa
Group:   System Environment/Daemons
Summary: A RRDtool Frontend For Various System Statistics for the BOA HTTP server.
Requires: boa
Requires: systemgraph = %{version}-%{release}
Provides: systemgraph-webconfig

%description boa
This package holds the systemgraph.cgi file and places it in boa's default
cgi-bin (%{_boaCgiDir}) directory.

It is also possible to define an own time range for the global systemgraph
summary page: http://localhost/cgi-bin/systemgraph.cgi?43h
                   --> summary for the last 43 hours
              http://localhost/cgi-bin/systemgraph.cgi?43d
                   --> summary for the last 43 days


#.....................................................................
%prep
%setup -q

#.......................................................
%build

#........................................................
%clean
%{__rm} -rf %{buildroot}

#........................................................
%install
%{__rm} -rf %{buildroot}

mkdir -p %{buildroot}/%{_bindir}
install -m 755 rrd_net.pl      %{buildroot}/%{_bindir}
install -m 755 rrd_cpumem.pl   %{buildroot}/%{_bindir}
install -m 755 rrd_cpustat.pl  %{buildroot}/%{_bindir}
install -m 755 rrd_cpufreq.pl  %{buildroot}/%{_bindir}
install -m 755 rrd_hdstat.pl   %{buildroot}/%{_bindir}
install -m 755 rrd_disk.pl     %{buildroot}/%{_bindir}
install -m 755 rrd_lsof.pl     %{buildroot}/%{_bindir}
install -m 755 rrd_lsof.sh     %{buildroot}/%{_bindir}
install -m 755 rrd_loadavg.sh  %{buildroot}/%{_bindir}
install -m 755 rrd_process.sh  %{buildroot}/%{_bindir}
install -m 755 rrd_users.sh    %{buildroot}/%{_bindir}
install -m 755 rrd_privoxy.sh  %{buildroot}/%{_bindir}
install -m 755 rrd_ntpdrift.pl %{buildroot}/%{_bindir}
install -m 755 rrd_1minute.sh  %{buildroot}/%{_bindir}
install -m 755 rrd_5minute.sh  %{buildroot}/%{_bindir}
install -m 755 rrd_health.pl   %{buildroot}/%{_bindir}
install -m 755 rrd_hdtemp.pl   %{buildroot}/%{_bindir}
install -m 755 rrd_cg_mem.pl   %{buildroot}/%{_bindir}

install -m 755 rrd_iptraf.pl     %{buildroot}/%{_bindir}
install -m 755 rrd_shorewall*.pl  %{buildroot}/%{_bindir}

# add additional scripts here .....


mkdir -p %{buildroot}%{_wwwDir}
install -m 755 %{name}.cgi %{buildroot}%{_wwwDir}/%{name}.cgi
%{__perl} -pi -e 's|withRRDTOOL_GIF = 1;|withRRDTOOL_GIF = %{_withRRDTOOL_GIF};|'    %{buildroot}%{_wwwDir}/%{name}.cgi
%{__perl} -pi -e 's|dieOnParamErrors = 1;|dieOnParamErrors = %{_dieOnParamErrors};|' %{buildroot}%{_wwwDir}/%{name}.cgi
%{__perl} -pi -e 's|pngXPoints = 500;|pngXPoints = %{_pngXPoints};|'                 %{buildroot}%{_wwwDir}/%{name}.cgi
%{__perl} -pi -e 's|/var/lib/systemgraph|%{_rrdDir}|'                                %{buildroot}%{_wwwDir}/%{name}.cgi

# same for opensuse/fedora
mkdir -p %{buildroot}%{_sysconfdir}/cron.d
install -m 600 %{name}.cron.d  %{buildroot}%{_sysconfdir}/cron.d/
%{__perl} -pi -e 's|DATABASEDIR=/var/lib/systemgraph|DATABASEDIR=%{_rrdDir}|' %{buildroot}/%{_sysconfdir}/cron.d/%{name}.cron.d 

# same for opensuse/fedora
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
install %{name}.sysconfig %{buildroot}%{_sysconfdir}/sysconfig/

#.................
# stuff for %{apache}
mkdir -p %{buildroot}/%{_sysconfdir}/%{apache}/conf.d
install -m 644 %{name}.httpd.conf %{buildroot}/%{_sysconfdir}/%{apache}/conf.d/%{ownwwwDir}.conf
%{__perl} -pi -e 's|/var/www/sg|%{_wwwDir}|' %{buildroot}/%{_sysconfdir}/%{apache}/conf.d/%{ownwwwDir}.conf

# modify tmp directory definition to avoid conflicts with other webservers
%{__perl} -pi -e 's|/tmp/systemgraph|/tmp/systemgraph_%{apache}|' %{buildroot}%{_wwwDir}/%{name}.cgi

#.................
# for boa
mkdir -p %{buildroot}%{_boaCgiDir}
cp %{buildroot}%{_wwwDir}/%{name}.cgi  %{buildroot}%{_boaCgiDir}
# modify tmp directory definition to avoid conflicts with other webservers
%{__perl} -pi -e 's|/tmp/systemgraph_%{apache}|/tmp/systemgraph_boa|' %{buildroot}%{_boaCgiDir}/%{name}.cgi




#..............................................................................
%post
echo "*******"
echo "* Note: you have to modify %{_sysconfdir}/sysconfig/%{name}.sysconfig"
echo "*******"

# reload %{crondaemon}
if [ $1 -eq 1 ] ; then
        /sbin/service %{crondaemon} reload > /dev/null 2>&1
fi
exit 0

%post %{apache}
# reload apache
if [ $1 -eq 1 ] ; then
        /sbin/service %{apache}     reload > /dev/null 2>&1
fi
exit 0

#.........................................................
%postun %{apache}
/sbin/service %{apache} reload     > /dev/null 2>&1
exit 0

%postun
/sbin/service %{crondaemon} reload > /dev/null 2>&1
exit 0

#.....................................................................
%files
%defattr(-,root,root,-)
%doc LICENSE README.md ChangeLog iptraf*png accounting TODO local.conf.addon
%attr(0755,root,root) %{_bindir}/rrd_*.pl
%attr(0755,root,root) %{_bindir}/rrd_*.sh
%attr(0600,root,root) %config(noreplace) %{_sysconfdir}/sysconfig/%{name}.sysconfig
%attr(0600,root,root) %config %{_sysconfdir}/cron.d/%{name}.cron.d

%files %{apache}
%defattr(-,root,root,-)
%attr(0755,root,root) %{_wwwDir}/%{name}.cgi
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/%{apache}/conf.d/%{ownwwwDir}.conf

%files boa
%defattr(-,root,root,-)
%dir %{_wwwDir}
%attr(0755,root,root) %{_boaCgiDir}/%{name}.cgi



#....................................................................
%changelog
* Wed Jun 13 2018 Jochen Schlick <> 0:0.9.8-50.js.os
- repo now on gitlab.com
- more sensors

* Wed Nov 19 2014 Jochen Schlick <> 0:0.9.8-49.js.os
- spec platform fixes

* Sat Nov  1 2014 Jochen Schlick <> 0:0.9.8-48.js.os
- repo now on github

* Wed Oct  6 2010 Jochen Schlick <> 0:0.9.7.6-48.js.os
- using new suse/fedora variables - should be more portable than the old one

* Thu Mar 11 2010 Jochen Schlick <> 0:0.9.7.6-47.js.os
- fedora boa version uses different cgi-bin

* Sat Feb 13 2010 Jochen Schlick <> 0:0.9.7.6-46.js.os
- additional sensors

* Mon Oct 26 2009 Jochen Schlick <> 0:0.9.7.5-45.js.os
- additional files

* Sun Jun 15 2008 Jochen Schlick <> 0:0.9.7.3-44.js.os
- VERSION_PLACEHOLDER for script

* Sun Feb 10 2008 Jochen Schlick <> 0:0.9.7.2-43.js.os
- post scripts modified

* Sun Feb 10 2008 Jochen Schlick <> 0:0.9.7.2-42.js.os
- update to 0.9.7.2

* Sat Nov 10 2007 Jochen Schlick <> 0:0.9.7.1-41.js.os
- update to 0.9.7.1 (hdstat fixes)

* Thu Nov  1 2007 Jochen Schlick <> 0:0.9.7-41.js.os
- update to 0.9.7

* Tue Oct 18 2007 Jochen Schlick <> 0:0.9.7-40.js.os
- hdstat added

* Sun Jun  3 2007 Jochen Schlick <> 0:0.9.6-39.js.os
- tar-file is now tar.bz2, some gentoo stuff
- rrd_lsof.sh added

* Thu May  3 2007 Jochen Schlick <> 0:0.9.5-38.js.os
- common spec file for fedora and opensuse (additional variables apache, crondaemon)

* Thu Mar 15 2007 Jochen Schlick <> 0:0.9.5-37.js.os
- after providing a boa rpm for opensuse the systemgraph boa package
  is now available.

* Sat Mar 10 2007 Jochen Schlick <> 0:0.9.5-36.js.fc
- rebuild

* Thu Mar  8 2007 Jochen Schlick <> 0:0.9.5-35.js.fc
- performance + nfs disks

* Mon Mar  5 2007 Jochen Schlick <> 0:0.9.5-34.js.fc
- cpufreq added

* Mon Feb 26 2007 Jochen Schlick <> 0:0.9.5-33.js
- boa directory entry added

* Thu Feb  8 2007 Jochen Schlick <> 0:0.9.5-32.js
- rrd_users.sh added

* Wed Dec 20 2006 Jochen Schlick <> 0:0.9.4-31.js
- cpu activity statistic added

* Wed Oct  4 2006 Jochen Schlick <> 0:0.9.4-30.js
- cgi bugfix

* Tue Oct  3 2006 Jochen Schlick <> 0:0.9.3-30.js
- extended device names for disk devices so that systemgraph.cgi
  is able to display the mountpoint or a other useful name for the
  disk device.
- some bug files

* Fri Sep 29 2006 Jochen Schlick <> 0:0.9.2-29.js
- rrd_hdtemp.pl added (harddisk temperature monitoring)
- testmode feature for rrd_health.pl

* Wed Sep 27 2006 Jochen Schlick <> 0:0.9.2-28.js
- additional health chip (thanx to Brad Lhotsky)

* Mon Sep 11 2006 Jochen Schlick <> 0:0.9.1-28.js
- clean rebuilt

* Sun Aug 27 2006 Jochen Schlick <> 0:0.9.1-27.js
- package split: we have now systemgraph, systemgraph-httpd, systemgraph-boa.
- the systemgraph.cgi file is now part of the systemgraph-httpd and
  the systemgraph-boa package whereas the database creation, data collection
  is still part of the systemgraph package.

* Thu Aug 24 2006 Jochen Schlick <> 0:0.9-26.js
- ntpdrift uses now the DJB tools (sntpclock,clockview) in case of
  ntpq is not installed

* Tue Aug 22 2006 Jochen Schlick <> 0:0.9-25.js
- new built

* Fri Aug 18 2006 Jochen Schlick <> 0:0.9-24.js
- systemgraph.cgi is now able to use different fonts for the graphs
- backgound color changed

* Wed Aug 16 2006 Jochen Schlick <> 0:0.9-23.js
- shorewall stuff included, so that the user has the chance to get rid
  of the iptraf tool. since the iptables based shorewall provides the
  same protocol statistic data.
- rrd databases changed, number of average/max values changed.
  most of the old rrd databases are now incompatible :-(

* Sat Aug 12 2006 Jochen Schlick <> 0:0.8.1-22.js
- rrd_privoxy.sh added

* Thu Aug 10 2006 Jochen Schlick <> 0:0.8-21.js
- health stuff reworked. health.rrd.sensorname split in fan.rrd.x.sensorname
  and temp.rrd.y.chipname (x,y number of fans, temperatures).
- temperatures are now accurate to a tenth degree 
- added iptraf.settings.png

* Mon Aug  7 2006 Jochen Schlick <> 0:0.7.2-20.js
- added new health chip (via686a-isa-6000) and some performance
  patches from Thierry Daucourt  

* Sun Jul 30 2006 Jochen Schlick <> 0:0.7.1-19.js
- mkdir also doesn't like tainted variables

* Sun Jul 30 2006 Jochen Schlick <> 0:0.7.1-18.js
- spec file bugfix

* Fri Jul 28 2006 Jochen Schlick <> 0:0.7.1-17.js
- cgi security patch and additional health chip (w83697hf-isa-0290)
  from Vincent Deffontaines.
- systemgraph.cgi in taint mode.
- spec file is now able to modify more systemgraph.cgi variables/config
  options

* Tue May  2 2006 Jochen Schlick <> 0:0.7-16.js
- protocol statistic using iptraf added

* Wed Feb  8 2006 Jochen Schlick <> 0:0.6-15.js
- ready for rrdtool 1.2.12

* Tue Feb 7 2006 Jochen Schlick <> 0:0.6
- ready for fedora core 5

* Tue Feb 22 2005 Jochen Schlick <> 0:0.5
- bugfix printHtml - cpu details lost 

* Mon Jan 31 2005 Jochen Schlick <> 0:0.5
- using lm_sensors for system health checking
- at the moment only my own winbond chip w83781d_i2c_0_2d (3 x fan,
  3 x temperature ) supported but it shoudn't be very difficult to
  integrate other sensor chips.

* Thu Dec  2 2004 Jochen Schlick <> 0:0.4
- rrd_memory.pl rrd_cpu.pl replaced by rrd_cpumem.pl
- rrd_cpumem.pl need current version of vmstat which allows the option -s
- rrd_ntpdrift.pl runs now every five minutes

* Wed Dec  1 2004 Jochen Schlick <> 0:0.4
- additional net infos, rrd_cpu.pl added, memory stuff added

* Sun Nov 28 2004 Jochen Schlick <> 0:0.3
- cgi script heavily modified
- it is now possible to define an own time range for the global summary page
  "systemgraph.cgi?43h" --> summary for the last 43 hours
  "systemgraph.cgi?43d" --> summary for the last 43 days
  (special feature for Uli)

* Fri Nov 26 2004 Jochen Schlick <> 0:0.2
- bug fix: show ntpdrift again

* Wed Nov 24 2004 Jochen Schlick <> 0:0.2
- rrd_net.pl added
- hides ntpdrift stuff when ntp is not configured
- rrd_1miunte.sh, rrd_5minute.sh, rrd_ntpdrift.pl added
- systemgraph.sysconfig syntax modified

* Tue Nov 23 2004 Jochen Schlick <> 0:0.1
- lsof database added

* Mon Nov 22 2004 Jochen Schlick <> 0:0.1
- not defined as service yet
- initial version
