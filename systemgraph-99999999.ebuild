# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v3
# $Id$

EAPI=6

WEBAPP_MANUAL_SLOT="yes"

inherit git-r3 eutils webapp

DESCRIPTION="A RRDtool Frontend For Various System Statistics"
HOMEPAGE="http://www.decagon.de/sw/systemgraph/"
EGIT_REPO_URI="https://gitlab.com/idi1806/systemgraph.git"

LICENSE="GPL-3"
KEYWORDS="~amd64 ~ia64 ~ppc ~sparc ~x86"
SLOT="0"

#............................................................
RDEPEND="dev-lang/perl
	>=net-analyzer/rrdtool-1.2.2[perl,graph]
	virtual/cron"


DEPEND="sys-apps/sed"

#............................................................
# my variables
MY_RRDDATABASEDIR="/var/lib/${PN}"
MY_CONFD="/etc/conf.d"
MY_CROND="/etc/cron.d"
MY_TMPDIR="/tmp/${PN}"

# only used when boa is installed
MY_BOA_CGIBINDIR="/var/www/localhost/cgi-bin"



#............................................................
pkg_setup() {
	webapp_pkg_setup

	# we need the rrdtool's perl module
	#built_with_use net-analyzer/rrdtool perl \
	#	|| die "net-analyzer/rrdtool must be built with USE=perl"
}

#............................................................
src_compile() {

	einfo "make systemgraph a real gentoo application:"

	einfo "use ${MY_CONFD} instead of /etc/sysconfig for ${PN}'s config file"
	sed -e "s|/etc/sysconfig|${MY_CONFD}|g" -i *.pl || \
		die "sed for perl files failed"
	sed -e "s|/etc/sysconfig|${MY_CONFD}|g" -i *.sh || \
		die "sed for shell files failed"

	einfo "${PN}'s RRD database directory is ${MY_RRDDATABASEDIR}"
	sed -e "s|/var/lib/systemgraph|${MY_RRDDATABASEDIR}|g" -i *.cgi || \
		die "sed for cgi files failed"
	sed -e "s|/var/lib/systemgraph|${MY_RRDDATABASEDIR}|g" -i *.cron.d || \
		die "sed for crond.d files failed"

	einfo "use rrdtool gif or not (=NOT)"
	sed -e "s|withRRDTOOL_GIF = 1;|withRRDTOOL_GIF = 0;|g" -i *.cgi || \
		die "sed for cgi files failed"

}

#............................................................
src_install() {
	webapp_src_preinst

	# be sure to run webapp_src_install *before* doing the
	# directories below because it cripples all other permissions :-(
	webapp_src_install

	einfo "creating directory for the RRD data files..."
	dodir /var/lib
	diropts -oroot -groot -m0755
	dodir ${MY_RRDDATABASEDIR}
	keepdir ${MY_RRDDATABASEDIR}

	einfo "copy rrd_* scripts..."
	dobin rrd_*

	einfo "create ${PN}.cgi in ${MY_CGIBINDIR}"
	exeinto ${MY_CGIBINDIR}
	exeopts -m0755
	doexe ${PN}.cgi

	einfo "copy conf-file(${PN}.sysconfig) to ${MY_CONFD}"
	doconfd ${PN}.sysconfig
	fperms 0600 ${MY_CONFD}/${PN}.sysconfig

	einfo "copy ${PN}'s docs....."
	dodoc README.md LICENSE ChangeLog iptraf.settings.png ${PN}.httpd.conf TODO* local.conf*

	einfo "create entry in ${MY_CROND}"
	dodir ${MY_CROND}
	insopts -m0600
	insinto ${MY_CROND}
	doins ${PN}.cron.d

	# boa specfic stuff.....................................
	if has_version 'www-servers/boa' ; then
		einfo "boa is installed, put also ${PN}.cgi in boa's cgi-bin directory, "

		einfo "boa's ${PN}.cgi uses ${MY_TMPDIR}.boa/ !!"
		sed -e "s|/tmp/systemgraph|${MY_TMPDIR}_boa|g" -i ${PN}.cgi || \
			die "sed for perl files failed"

		dodir ${MY_BOA_CGIBINDIR}/
		exeinto ${MY_BOA_CGIBINDIR}/
		exeopts -m0755
		doexe ${PN}.cgi
	fi
}


#............................................................
pkg_postinst() {

	elog

	# compatibility for older ebuilds
	if [[ -d ${ROOT}/var/www/sg ]] ; then
		mv -f ${ROOT}/var/www/sg/*  ${ROOT}${MY_RRDDATABASEDIR} \
			&& rmdir ${ROOT}/var/www/sg
		elog "${PN} rrd files moved from /var/www/sg to ${MY_RRDDATABASEDIR}"
		elog
	fi

	# cpumem.rrd is now obsolete (>= 0.9.7)
	if [[ -f ${ROOT}${MY_RRDDATABASEDIR}/cpumem.rrd ]] ; then
		rm -f ${ROOT}${MY_RRDDATABASEDIR}/cpumem.rrd
		elog "${PN} obsolete rrd file cpumem.rrd removed"
		elog
	fi

	elog "Some statistic values are only available if you have installed the"
	elog "concerning tools. Sea README.md for details."
	elog "Remember to edit ${MY_CONFD}/${PN}.sysconfig to suit"
	elog "your needs (adding network devives, partitions...)."
	elog
}
